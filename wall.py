import sfml as sf
import math

class Wall(object):
    def __init__(self, startpos, endpos):
        # sf.Vector2:
        self.startpos = startpos
        self.endpos = endpos
        
        self.wall = sf.RectangleShape()
        # Length of the wall
        self.length = math.sqrt(math.pow((startpos.y-endpos.y),2)+math.pow((startpos.x-endpos.x),2))
        self.wall.size = (self.length, 10)
        # Unit vector
        self.unitvector = sf.Vector2((endpos.x-startpos.x)/self.length,(endpos.y-startpos.y)/self.length)
        
        # Calculate rotation for wall.
        if self.endpos.x-self.startpos.x == 0:
            if self.endpos.y-self.startpos.y < 0:
                rotation = 270
            else: rotation = 90
        else:
            y = self.endpos.y-self.startpos.y
            x = self.endpos.x-self.startpos.x
            rotation = math.degrees(math.atan(y/x))
            if x < 0: rotation += 180
            elif y < 0: rotation += 360
            
        self.wall.rotate(rotation)
        self.wall.outline_color = sf.Color.BLACK
        self.wall.fill_color = sf.Color.RED
        self.wall.outline_thickness = 1
        xDif, yDif = 0, 0
        # Adjust the drawing position roughly based on rotation
        if rotation > 315 or rotation < 45 or (rotation > 135 and rotation < 225): xDif = 5
        if (rotation < 315 and rotation > 225) or (rotation > 45 and rotation < 135): yDif = 5
        self.wall.position = sf.Vector2(startpos.x + xDif, startpos.y + yDif)
        
    
    def getStartpos(self):
        return self.startpos
        
    def getEndpos(self):
        return self.endpos
        
    def getUnitvector(self):
        return self.unitvector
        
    def getRotation(self):
        return self.wall.rotation
    
    def draw(self, rt):
        # rt: RenderTarget
        rt.draw(self.wall)