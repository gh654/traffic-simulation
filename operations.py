import sfml as sf
import math

# Math operations

def calcSteerVel(desired_velocity, velocity, max_accel):
    # Calculate steering velocity towards the desired velocity
    steer = sf.Vector2(desired_velocity.x - velocity.x, desired_velocity.y - velocity.y)
    # Steering truncation to max acceleration:
    if steer.x > max_accel:
        steer.x = max_accel
    elif steer.x < -max_accel:
        steer.x = -max_accel
    if steer.y > max_accel:
        steer.y = max_accel
    elif steer.y < -max_accel:
        steer.y = -max_accel
    # Add steering acceleration to current velocity
    vel = sf.Vector2((velocity.x + (steer.x)), (velocity.y + (steer.y)))
    return vel
    
    
def calcPointAngle(startp, endp, currRot, FOV):
    # This function is designed for checking if a point is inside the object's field of view from where it is facing
    # Calculate rotation from startpoint to endpoint, keep the angle comparable to desired FOV from currRot
    # startp, endp: sf.Vector2
    # Return: pointAngle is the angle difference from current angle to the end point
    if endp.x - startp.x == 0:
        if endp.y - startp.y < 0:
            # The following keeps the angle as negative if velocity rotation is below the FOV angle, more below
            # (With small angles, <90, has no effect)
            if currRot < FOV: pointAngle = -90
            else: pointAngle = 270
        else: pointAngle = 90
    else:
        pointY = endp.y-startp.y
        pointX = endp.x-startp.x
        pointAngle = math.degrees(math.atan(pointY/pointX))
        if pointX < 0: pointAngle += 180
        # The following keeps the angle as negative if goal rotation is below the FOV angle,
        # because the algorithm needs it for cases where FOV goes over the zero degree area
        elif pointY < 0 and currRot > FOV: pointAngle += 360
        
    return pointAngle
    
    
def calcRotation(startp, endp):
    # Calculate a rotation from start point to end point in degrees 0-360
    # startp, endp: sf.Vector2
    if endp.x - startp.x == 0:
        if endp.y - startp.y < 0:
            rotation = 270
        else: rotation = 90
    else:
        pointY = endp.y - startp.y
        pointX = endp.x - startp.x
        rotation = math.degrees(math.atan(pointY/pointX))
        if pointX < 0: rotation += 180
        elif pointY < 0: rotation += 360
        
    return rotation
    
    
def calcVecRot(vec):
    # Calculate a vector's rotation
    # vec: sf.Vector2
    if vec.x == 0:
        if vec.y < 0:
            rotation = 270
        else: rotation = 90
    else:
        pointY = vec.y
        pointX = vec.x
        rotation = math.degrees(math.atan(pointY/pointX))
        if pointX < 0: rotation += 180
        elif pointY < 0: rotation += 360
        
    return rotation