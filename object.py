import sfml as sf
import math

# Simulation's moving objects
class Object(object):
    def __init__(self, position):
        # Position: sf.Vector2
        self.obj = sf.CircleShape(5, 50)
        self.obj.position = position
        self.obj.fill_color = sf.Color.YELLOW
        self.obj.outline_color = sf.Color.BLACK
        self.obj.outline_thickness = 1
        
        self.velocity = sf.Vector2(0,50)
        
    def getPosition(self):
        return self.obj.position
        
    def getVelocity(self):
        return self.velocity
        
    def setVelocity(self, v):
        self.velocity = v
    
    def getRotation(self): # Velocity vector's rotation
        if self.velocity.x == 0:
            if self.velocity.y < 0: return 270
            else: return 90
        rot = math.degrees(math.atan(self.velocity.y/self.velocity.x))
        if self.velocity.x < 0: rot += 180
        elif self.velocity.y < 0: rot += 360
        return rot
        
    def draw(self, rt):
        # rt: RenderTarget
        rt.draw(self.obj)

    def update(self, dt):
        self.obj.move(sf.Vector2(dt * self.velocity.x, dt * self.velocity.y))
        
        
# To simplify text handling in code:
class Text(object):
    def __init__(self, string, position, size):
        font = sf.Font.from_file("C:\Windows\Fonts\Arial.ttf")
        self.Txt = sf.Text(string)
        self.Txt.font = font
        self.Txt.position = position
        self.Txt.character_size = size
        if string == "Spawning paused": self.hidden = True
        else: self.hidden = False
    def setString(self, string):
        self.Txt.string = string
    def setColor(self, color):
        self.Txt.color = color
    def isHidden(self):
        # Text is hidden if it comes up only when spawning has paused
        return self.hidden
    '''
    def __repr__(self):
        # returns the text object for drawing
        return self.Txt
    '''
    def get(self):
        # returns the text object for drawing
        return self.Txt