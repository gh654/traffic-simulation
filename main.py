import sfml as sf
import os
import sys
import math

from object import *
from wall import Wall
from operations import *

def start():
    window = sf.RenderWindow(sf.VideoMode(300, 100), "Startup")
    
    infoTxt = Text("Enter filename for simulation:", (5,5), 20)
    fileTxt = Text("", (5, 40), 20)
    fileErrorTxt = Text("Invalid file name!", (5, 70), 20)
    fileErrorTxt.setColor(sf.Color.RED)
    dirList = os.listdir(os.path.join(sys.path[0], "simulations"))
    input = dirList[0]
    dirListSize = len(dirList)
    i = 0
    fileError = 0

    while window.is_open:
        for event in window.events:
            # close window: exit
            if type(event) is sf.CloseEvent:
                window.close()
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.UP):
                i += 1
                if i == dirListSize: i = 0
                input = dirList[i]
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.DOWN):
                i -= 1
                if i < 0: i = dirListSize - 1
                input = dirList[i]
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.BACK_SPACE):
                input = input[:-1]
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.RETURN):
                if os.path.isfile(os.path.join(sys.path[0], "simulations", input)) == False:
                    fileError = 1
                else:
                    fileError = 0
                if fileError == 0:
                    window.close()
                    simulation(input)
            elif type(event) is sf.TextEvent and not sf.Keyboard.is_key_pressed(sf.Keyboard.BACK_SPACE) and not sf.Keyboard.is_key_pressed(sf.Keyboard.RETURN):
                input += ''.join(chr(event.unicode))
        #fileTxt.string = input
        fileTxt.setString(input)
        window.clear(sf.Color.BLACK)
        window.draw(infoTxt.get())
        window.draw(fileTxt.get())
        if fileError == 1:
            window.draw(fileErrorTxt.get())
        window.display()
                




def simulation(filename):
    MAX_SPEED = 200
    ACC = 10 # Acceleration
    SPR_ACC = 50 # Separation acceleration
    FLEE_RANGE = 50
    AVOID_RANGE = 80 # Range for reacting before hitting walls
    OFF_DIST = 20 # Offset pursuit point's distance from walls
    ARR_DIST = 20 # Braking arrival point's distance from other objects
    COLL_RANGE = 10 # Collision range
    SPR_SPEED = 50 # Speed for separating colliding objects
    GOAL_RANGE = 20 # From which distance the object reaches the goal
    FOV = 110 # Object's "field of view" in degrees, for walls
    FOV_OBJ = 30 # # Object's "field of view" in degrees, for other objects
    SPAWN_TIME = 2 # For creating objects
    WAVE_AMOUNT = 5 # How many objects are created per spawntime
    WAVE_SPEED = 0.2 # How quickly objects are spawned in a wave

    window = sf.RenderWindow(sf.VideoMode(800, 600), "Simulation")
    window.vertical_synchronization = True
    
    startPoint = sf.Vector2(0,0)
    
    objects = [] # Array of all moving objects in simulation
    walls = [] # Array of all static walls in simulation
    texts = [] # Array of text objects

    clock = sf.Clock() # For simulation mechanics
    spawnClock = sf.Clock() # For object spawning from startpoint
    waveClock = sf.Clock() # To separate objects at starting point while waves enabled
    objectsSpawned = 0 # Counter for objects spawned in current wave (cannot spawn simultaneously)
    spawnPause = 0 # To pause spawning
    x, y = 0, 0
    
    # Read the input file
    try:
        f = open(os.path.join(sys.path[0], "simulations", filename), "r")
    except IOError:
        print("Failed to open input file.")
        return

    lines = f.readlines()
    f.close()
    
    wallmode = 0
    parammode = 0
    startDefined = False # Must define starting point
    for line in lines:
        # Walls
        if line.strip() == "-WallBegin":
            if parammode == 1: break
            wallmode = 1
        elif line.strip() == "-WallEnd":
            wallmode = 0
        elif wallmode == 1:
            try:
                currline = line.strip().split("-")
                startcoord = currline[0].split(",")
                endcoord = currline[1].split(",")
            except IndexError:
                print("Failed to read input file: corrupted wall coordinate syntax")
                return
            try:
                walls.append(Wall(sf.Vector2(int(startcoord[0]), int(startcoord[1])), sf.Vector2(int(endcoord[0]), int(endcoord[1]))))
            except ValueError:
                print("Failed to read input file: invalid wall coordinate values")
                return   
        # Parameters
        elif line.strip() == "-ParametersBegin":
            if wallmode == 1: break
            parammode = 1
        elif line.strip() == "-ParametersEnd":
            parammode = 0
        elif parammode == 1:
            try:
                currline = line.strip().split()
                paramData = currline[1].split(",")
            except IndexError:
                print("Failed to read input file: corrupted parameters")
                return
            try:
                if currline[0] == "StartPoint":
                    startPoint = sf.Vector2(int(paramData[0]), int(paramData[1]))
                    startDefined = True
                elif currline[0] == "SpawnTime":
                    SPAWN_TIME = int(paramData[0])
                elif currline[0] == "WaveAmount":
                    WAVE_AMOUNT = int(paramData[0])
                elif currline[0] == "WaveSpeed":
                    WAVE_SPEED = float(paramData[0])
            except (ValueError, IndexError):
                print("Failed to read input file: Parameters incorrect")
                return
            
    if wallmode == 1:
        print("Failed to read input file: No -WallEnd definition")
        return
    if parammode == 1 or not startDefined:
        print("Failed to read input file: missing parameters or invalid parameters syntax")
        print("Need parameter for start point")
        return

    
    # Debug data:
    debugClock = sf.Clock()
    distcalc = 0 # Amount of distance calculations
    
    # Texts:
    debugTxt = Text("Distance calculations / second: ", (20,500), 15)
    amountTxt = Text("Number of objects: ", (20,525), 15)
    fpsTxt = Text("FPS", (20,550), 15)
    pauseTxt = Text("Spawning paused", (20,450), 20)
    fleeTxt = Text("Q/A: Flee Range " + str(FLEE_RANGE), (575,500), 15)
    avoidTxt = Text("E/D: Avoid Range " + str(AVOID_RANGE), (575,525), 15)
    offsetTxt = Text("T/G: Offset Point Range " + str(OFF_DIST), (575,550), 15)
    speedTxt = Text("Up/Down: Max Speed " + str(MAX_SPEED), (325,525), 15)
    accTxt = Text("Left/Right: Max Acceleration " + str(ACC), (325,550), 15)
    texts = [debugTxt, amountTxt, fpsTxt, pauseTxt, fleeTxt, avoidTxt, offsetTxt, speedTxt, accTxt]

    fps = 0

    while window.is_open:
        # Event tracker
        for event in window.events:
            # close window: exit
            if type(event) is sf.CloseEvent:
                window.close()
            # Mouse moved -> coordinates to x and y
            elif event == sf.MouseMoveEvent:
                x, y = event.position
            # Buttons pressed -> modify values
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.UP):
                MAX_SPEED += 1
                speedTxt.setString("Up/Down: Max Speed " + str(MAX_SPEED))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.DOWN):
                MAX_SPEED -= 1
                speedTxt.setString("Up/Down: Max Speed " + str(MAX_SPEED))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.RIGHT):
                ACC += 1
                accTxt.setString("Left/Right: Max Acceleration " + str(ACC))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.LEFT):
                ACC -= 1
                accTxt.setString("Left/Right: Max Acceleration " + str(ACC))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.Q):
                FLEE_RANGE += 1
                fleeTxt.setString("Q/A: Flee Range " + str(FLEE_RANGE))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.A):
                FLEE_RANGE -= 1
                fleeTxt.setString("Q/A: Flee Range " + str(FLEE_RANGE))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.E):
                AVOID_RANGE += 1
                avoidTxt.setString("E/D: Avoid Range " + str(AVOID_RANGE))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.D):
                AVOID_RANGE -= 1
                avoidTxt.setString("E/D: Avoid Range " + str(AVOID_RANGE))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.T):
                OFF_DIST += 1
                offsetTxt.setString("T/G: Offset Point Range " + str(OFF_DIST))
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.G):
                OFF_DIST -= 1
                offsetTxt.setString("T/G: Offset Point Range " + str(OFF_DIST))
            # Space pressed -> Pause spawning
            elif type(event) is sf.KeyEvent and sf.Keyboard.is_key_pressed(sf.Keyboard.SPACE):
                if spawnPause == 0: spawnPause = 1
                else: spawnPause = 0
        # Clocks:
        fps += 1
        # Debug clock for calculations per sec
        if debugClock.elapsed_time >= sf.seconds(1):
            debugClock.restart()
            debugTxt.setString("Distance calculations / second: " + str(distcalc))
            fpsTxt.setString("FPS " + str(fps))
            fps = 0
            amountTxt.setString("Number of objects: " + str(len(objects)))
            distcalc = 0
            
        # Spawn clock
        if spawnClock.elapsed_time >= sf.seconds(SPAWN_TIME) and not spawnPause:
            if objectsSpawned == WAVE_AMOUNT:
                objectsSpawned = 0
                spawnClock.restart()
            if waveClock.elapsed_time >= sf.seconds(WAVE_SPEED):
                objectsSpawned += 1
                waveClock.restart()
                objects.append(Object(startPoint))
            
        # dt: time between frames
        dt = clock.restart().seconds
        
        # Ball radius is hard coded to 5 pixels
        
        for obj in objects: # Calculate vectors for all objects in simulation
            fled = False # To check if object has to flee
            avoid = False # To check if object is avoiding a wall
            brake = False # To check if object is braking for another object in front of it
            collision = False # To check if collided
            # Current position
            cx = obj.getPosition().x + 5
            cy = obj.getPosition().y + 5
            # Current velocity
            vel = obj.getVelocity() #sf.Vector2(x,y)
            fleeVel = sf.Vector2(0,0) # Needed for checking if wall is in the way of flee vector (don't flee into walls)
            objRot = obj.getRotation() # Velocity vector's rotation

            # Distance to the goal
            goaldist = math.sqrt(math.pow(x - cx, 2) + math.pow(y - cy, 2))
            distcalc += 1 # DEBUG
            if goaldist <= GOAL_RANGE:
                objects.remove(obj)
                break
            
            for obj2 in objects: # Comparison to all other objects for flee mode
                if obj2 == obj: # Do not calculate to self
                    continue
                pos2 = sf.Vector2(obj2.getPosition().x + 5, obj2.getPosition().y + 5)
                dist = math.sqrt(math.pow(cx - pos2.x, 2) + math.pow(cy - pos2.y, 2))
                distcalc += 1 # DEBUG
                react = False # Reaction flag for avoidance
                
                if dist <= COLL_RANGE: # Check collision
                    if dist == 0: # Avoid division by zero
                        # Point current velocity to opposite direction
                        vel = sf.Vector2(-vel.x, -vel.y)
                        # Flag as fled
                        fled = True
                        continue
                    if not collision:
                        # Reset current velocity once for correct calculation
                        # for collision vector in case of multiple objects colliding
                        collision = True
                        vel = sf.Vector2(0,0)
                    # Calculate (negative) desired velocity vector
                    # Add enough velocity to separate colliding objects
                    desvel = sf.Vector2(((pos2.x - cx) / dist)*SPR_SPEED, ((pos2.y - cy) / dist)*SPR_SPEED)
                    # Point desired velocity to opposite direction, outwards from the other object
                    desvel = sf.Vector2(-desvel.x,-desvel.y)
                    # Calculate steering velocity: steering = desired_velocity - velocity
                    vel = calcSteerVel(desvel, vel, SPR_ACC)

                    # Flag as fled
                    fled = True
                    
                
                elif dist <= AVOID_RANGE and dist > ARR_DIST: # For arrival behaviour, braking when an object is in the way
                    # Brakes when other object is in FOV_OBJ angle at the velocity vector direction
                    # Calculate rotation to the other object
                    pointAngle = calcPointAngle(sf.Vector2(cx, cy), pos2, objRot, FOV_OBJ)

                    # Check if point is in FOV_OBJ and current speed is towards the desired location
                    #print(goalRot - (FOV/2), pointAngle, goalRot + (FOV/2))
                    if (pointAngle > (objRot - (FOV_OBJ/2)) and pointAngle < (objRot + (FOV_OBJ/2))) or (pointAngle+360 > (objRot - (FOV_OBJ/2)) and pointAngle+360 < (objRot + (FOV_OBJ/2))):
                        react = True
                    
                    if react:
                        
                        offdist = dist - ARR_DIST # 'dist' is now the distance to next to the other object, not into it
                        if offdist < 0: offdist = 0
                        ramped_speed = MAX_SPEED * (offdist / AVOID_RANGE)
                        #print (offdist)
                        # Calculate desired velocity vector for ARRIVAL behaviour braking ARR_DIST distance before other object
                        # Desired direction is still towards mouse
                        if goaldist != 0:
                            desvel = sf.Vector2(((ramped_speed / goaldist) * (pos2.x - cx)), ((ramped_speed / goaldist) * (pos2.y - cy)))
                        else:
                            desvel = sf.Vector2(0,0)
                            
                        # Point desired velocity to opposite direction, outwards from the other object
                        desvel = sf.Vector2(-desvel.x,-desvel.y)
                        # Calculate steering velocity: steering = desired_velocity - velocity
                        fleeVel = calcSteerVel(desvel, fleeVel, ACC)

                        # Flag as braked
                        brake = True
                
                    
                #if dist <= FLEE_RANGE and (not react or dist < ARR_DIST):
                if dist <= FLEE_RANGE and not react and not fled:
                    # If distance to another object is less than flee range
                    # Flee behaviour also if braking and still too close to another object
                    # Calculate and normalize (negative) desired velocity vector to maximum speed
                    desvel = sf.Vector2(((pos2.x - cx) / dist)*MAX_SPEED, ((pos2.y - cy) / dist)*MAX_SPEED)
                    # Point desired velocity to opposite direction, outwards from the other object
                    desvel = sf.Vector2(-desvel.x,-desvel.y)
                    # Calculate steering velocity: steering = desired_velocity - velocity
                    fleeVel = calcSteerVel(desvel, fleeVel, ACC)

                    # Flag as fled
                    fled = True
                    
            # Desired rotation towards goal
            goalRot = calcRotation(sf.Vector2(cx, cy), sf.Vector2(x, y))

            # Rotation of the flee vector
            fleeRot = calcVecRot(fleeVel)
            
            closestWall = AVOID_RANGE # Distance to the nearest wall. Don't calculate farther walls
            for wall in walls: # Comparison to all walls for avoid mode
                
                wstart = wall.getStartpos()
                wend = wall.getEndpos()
                # Calculate the closest point of the wall to the object
                unit = wall.getUnitvector()         
                # Coordinates of the position
                vektori = (wstart - sf.Vector2(cx,cy))
                pistetulo = (vektori.x*unit.x+vektori.y*unit.y)
                pos2 = sf.Vector2(cx,cy) + vektori - sf.Vector2(pistetulo*unit.x,pistetulo*unit.y)
                
                # Flag that is true, if wall's current closest point is its start or end point.
                # In that case don't run avoid behaviour because it may bug.
                noAvoid = False
                # Closest point truncation into walls
                if pos2.x < wstart.x:
                    if wstart.x < wend.x:
                        noAvoid = True
                        pos2.x = wstart.x
                if pos2.x > wstart.x:
                    if wstart.x > wend.x:
                        noAvoid = True
                        pos2.x = wstart.x
                if pos2.y < wstart.y:
                    if wstart.y < wend.y:
                        noAvoid = True
                        pos2.y = wstart.y
                if pos2.y > wstart.y:
                    if wstart.y > wend.y:
                        noAvoid = True
                        pos2.y = wstart.y
                if pos2.x > wend.x:
                    if wend.x > wstart.x:
                        noAvoid = True
                        pos2.x = wend.x
                if pos2.x < wend.x:
                    if wend.x < wstart.x:
                        noAvoid = True
                        pos2.x = wend.x
                if pos2.y > wend.y:
                    if wend.y > wstart.y:
                        noAvoid = True
                        pos2.y = wend.y
                if pos2.y < wend.y:
                    if wend.y < wstart.y:
                        noAvoid = True
                        pos2.y = wend.y
                        
                # Distance to that point
                dist = math.sqrt(math.pow(cx - pos2.x, 2) + math.pow(cy - pos2.y, 2))
                distcalc += 1 # DEBUG
                
                if dist < closestWall:
                    closestWall = dist
                else:
                    continue
                
                # Calculate angle from the object to closest point on the wall.
                # From that angle, calculate if closest point is inside the "field of view" of the object
                # e.g. if the object reacts to the wall or not.
                react = False # Reaction flag
                
                # Rotation to point on the wall
                pointAngle = calcPointAngle(sf.Vector2(cx, cy), pos2, goalRot, FOV)

                # Check if point is in FOV and goal rotation is towards the desired location
                if (pointAngle > (goalRot - (FOV/2)) and pointAngle < (goalRot + (FOV/2))) or (pointAngle+360 > (goalRot - (FOV/2)) and pointAngle+360 < (goalRot + (FOV/2))):
                    react = True

                if dist <= COLL_RANGE: # Check collision
                    if dist == 0: # Avoid division by zero
                        # Point current velocity to opposite direction
                        vel = sf.Vector2(-vel.x, -vel.y)
                        # Flag as fled
                        fled = True
                        continue
                    if not collision:
                        # Reset current velocity once for correct calculation
                        # for collision vector in case of multiple objects colliding
                        collision = True
                        vel = sf.Vector2(0,0)
                    # Calculate (negative) desired velocity vector
                    # Add enough velocity to separate colliding objects
                    desvel = sf.Vector2(((pos2.x - cx) / dist)*SPR_SPEED, ((pos2.y - cy) / dist)*SPR_SPEED)
                    # Point desired velocity to opposite direction, outwards from the other object
                    desvel = sf.Vector2(-desvel.x,-desvel.y)
                    # Add desired velocity to current velocity
                    vel = sf.Vector2((vel.x + (desvel.x)), (vel.y + (desvel.y)))
                    # Flag as fled
                    fled = True
                
                
                elif dist <= AVOID_RANGE and react and not noAvoid:
                    # If distance to a wall is in avoid distance
                    # Using unit vector, calculate offset pursuit point's coordinates
                    # Point's distance OFF_DIST pixels from wall
                    offpoint = sf.Vector2((pos2.x - (((pos2.x - cx) / dist)*OFF_DIST)), (pos2.y - (((pos2.y - cy) / dist)*OFF_DIST)))
                    # Distance to an offset pursuit point next to wall: 
                    offdist = dist - OFF_DIST
                    #print(offpoint)
                    if offdist < 0:
                        # If the object is closer to the wall than the offset point, add more velocity to separate from wall
                        ramped_speed = MAX_SPEED * (offdist/OFF_DIST)
                    else:
                        ramped_speed = MAX_SPEED * (offdist / AVOID_RANGE)
                    #print(ramped_speed)
                    # Calculate desired velocity vector for ARRIVAL behaviour into the offset pursuit point
                    if offdist != 0:
                        desvel = sf.Vector2(((ramped_speed / offdist) * (offpoint.x - cx)), ((ramped_speed / offdist) * (offpoint.y - cy)))
                    else:
                        desvel = sf.Vector2(0,0)
                    # Calculation: if object's goalRotation differs from wall's angle more than 90
                    # degrees, use wall's negative unit vector. Otherwise use the wall's current unit vector
                    # Take into account also the rotation the object wants to flee towards, calculated before with other objects.
                    rotDiffer = math.fabs(math.fabs(wall.getRotation())-math.fabs(goalRot))
                    if fleeRot == 90 or dist > OFF_DIST+5:
                        fleeRotDiffer = 90
                    else:
                        fleeRotDiffer = math.fabs(math.fabs(wall.getRotation())-math.fabs(fleeRot))
                    if rotDiffer > 180:
                        rotDiffer = 360 - rotDiffer
                    if fleeRotDiffer > 180:
                        fleeRotDiffer = 360 - fleeRotDiffer
                    rotDisagree = False
                    if math.fabs(math.fabs(goalRot)-math.fabs(fleeRot)) > 90 and fled: 
                        # If goal rotation and desired flee rotations disagree
                        rotDisagree = True
                        rotDiffer = fleeRotDiffer
                    if rotDiffer > 180:
                        rotDiffer = 360 - rotDiffer
                    if rotDiffer > 88 and rotDiffer < 92:
                        avoid_unit = sf.Vector2(0,0)
                    elif rotDiffer > 90:
                        avoid_unit = -unit
                    else: avoid_unit = unit
                    
                    # Add vector to steer object towards the calculated unit vector, or steer to flee direction
                    if rotDisagree:
                        desvel = sf.Vector2((desvel.x + (avoid_unit.x * (MAX_SPEED-ramped_speed))), (desvel.y + (avoid_unit.y * (MAX_SPEED-ramped_speed))))
                    else:
                        desvel = sf.Vector2((desvel.x + (avoid_unit.x * (MAX_SPEED-ramped_speed))), (desvel.y + (avoid_unit.y * (MAX_SPEED-ramped_speed))))
                    
                    # If the flee velocity is now taken into account, (distance is close to wall), reset it.
                    if dist <= OFF_DIST+5:
                        fleeVel = sf.Vector2(0,0)

                    # Calculate steering velocity: steering = desired_velocity - velocity
                    vel = calcSteerVel(desvel, vel, ACC)

                    # Flag as avoided
                    avoid = True
                

            #if not fled and not avoid: # Normal seeking situation
            if not avoid: # Normal seeking situation
                # Calculate and normalize desired velocity vector to maximum speed
                if goaldist == 0:
                    desvel = sf.Vector2(0,0)
                else:
                    desvel = sf.Vector2(((x - cx) / goaldist)*MAX_SPEED, ((y - cy) / goaldist)*MAX_SPEED)
                # Calculate steering velocity: steering = desired_velocity - velocity
                vel = calcSteerVel(desvel, vel, ACC)
                
                
            # Update object velocity
            vel = vel + fleeVel
            obj.setVelocity(vel)
            
            

        # Draw everything and update objects
        window.clear(sf.Color.BLUE)
        for obj in objects:
            obj.update(dt)
            obj.draw(window)
        for wall in walls:
            wall.draw(window)
        for text in texts:
            if text.isHidden():
                if not spawnPause: continue
            window.draw(text.get())

        window.display()

        
    
if __name__ == '__main__':
    start()
    #simulation()